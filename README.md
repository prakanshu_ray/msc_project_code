# MSC_Project_Code



## Description

This repository contains the project code used to benchmark the deep neural network architecture in predicting the MGMT promoter methylation status(methylated and un-methylated) using Brain Tumor MRI scans. The project is based on the task 2 of the challenge "Brain Tumor AI Challenge 2021" [https://www.kaggle.com/competitions/rsna-miccai-brain-tumor-radiogenomic-classification](https://www.kaggle.com/competitions/rsna-miccai-brain-tumor-radiogenomic-classification) organised by RSNA and MICCAI in July 2021. The project is executed using Kaggle notebook. Three architecture were used in benchmarking, these are ResNet-50, EfficientNet-B3, InceptionV3. The repository also contains the power point presentation containing results of the experiment conducted.


## Repository structure

For each architecture, there is a folder. So there are three folders.

1) EfficientNet-B3 
   - contains the python notebook file. This file contains the executed code 
     using EfficientNet-B3.

2) ResNet-50
   - contains the python notebook file. This file contains the executed code 
     using ResNet-50 architecture.

3) InceptionV3
   - contains the python notebook file. This file contains the executed code 
     using InceptionV3 architecture.

There is a power point presentation file containing the performance of three architectures in predicting MGMT promoter methylation status using brain tumor MRI scans.

## Benchmarking Parameters

- Recall value of model using each architecture
- Training time and number of layers
- AUC(Area under the ROC Curve)
- Performance of architecture on each modality(MRI Sequence)
